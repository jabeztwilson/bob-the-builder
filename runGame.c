// Version 1.4
// Created by Oliver Tan
// 19 May 2011
// Pits your AI against each other
// Must compile with Game.c and ai.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

// Game aspects
#define UNI_CHAR_NAME ('A' - UNI_A)
#define WINNING_KPI 150
#define DICE_AMOUNT 2

// runGame defaults
#define INVALID -1
#define DICE_FACES 6

#define NUM_DISCIPLINES 6

// Action:
#define ACTION_NAMES \
   { "Pass", "Build Campus", "Build GO8", "Obtain ARC", \
     "Start Spinoff", "", "", "Retrain Student" }

#define DISCIPLE_NAMES \
   { "ThD", "BPS", "BQN", "MJobs", "MTV", "M$" }

#define SCREEN_WIDTH 50
#define LINE_BREAK_SEPARATOR '-'

   
#define CYAN STUDENT_BQN
#define PURP STUDENT_MMONEY
#define YELL STUDENT_MJ
#define RED STUDENT_BPS
#define GREE STUDENT_MTV 
#define BLUE STUDENT_THD

#define MAX_PASS 9000

//defines for struct definition
 #define DIRECTIONS 4
 #define AREAS_PER_NODE 3
 #define NUM_NODES 54
 #define NUM_RESOURCES 6
 #define NUM_NODES_PER_AREA 6
 #define NUM_REGIONS 19

//directions
   #define UP 0
   #define LEFT 1
   #define DOWN 2
   #define RIGHT 3

//other important MACROS
   #define EVEN_ODD 1
   #define ODD_EVEN 0

   #define VALUE_FOUND 1
   #define VALUE_NOTFOUND 0

   #define MAX_POSSIBLE_PATH 11

//MACROS for path states
   #define BACK 2
   #define START 3
   #define FORWARD 4
   #define EXIT 9

   #define TRAINING_CENTRES {2,3,4,5,29,35,46,51,48,43}
   #define TRAINING_DISCIPLINES {STUDENT_MTV,STUDENT_MTV,\
                                             STUDENT_MMONEY,STUDENT_MMONEY,\
                                            STUDENT_BQN,STUDENT_BQN,\
                                            STUDENT_MJ,STUDENT_MJ,\
                                            STUDENT_BPS,STUDENT_BPS}
   #define MAX_RETRAIN_CENTRES 10
   #define NO_TRAINING 6
   #define START_OF_REGIONS {6,18,30,2,13,25,37,0,8,20,32,44,4,15,27,39,10,22,34}
   #define DIR_AROUND_HEX {RIGHT,DOWN,DOWN,LEFT,UP} 
//MACROS for printNetwork  
   #define GRID_ROW 11
   #define GRID_COL 12        
   #define UPPER 0
   #define MIDDLE 1
   #define LOWER 2  
   #define REP_VAC 'o'    
   #define REP_A '*'
   #define REP_B '+'
   #define REP_C 'x'    

#define MARK printf("MARKED\n");

#define NONE -1
#define DICE -2
#define DISC -3
#define NODE -4
 
//#define MAIN_TEST
#define MAIN_GAME

#define EXCESS_LINES 2

void randomDisciplines(int disciplines[]);
void randomDice(int dice[]);
void rigBoard(int disciplines[], int dice[]);
int rollDice(void);
int checkForWinner(Game g);
void printLineBreak(void);
void printBoard(Game g);

//Jabez functions and structs
//#################################################################
//BEGINNING OF STRUCT DEFINITIONS
//#################################################################
   typedef struct _node *nodePtr;
   typedef struct _node node;

   typedef struct _pathStack * PathStack;

   //node struct
      struct _node {
         //node Id of this node
         int nodeID;
         //next pointers to neghboring vertices
         nodePtr next, neighbour[DIRECTIONS];
         int training;
      };
   //path stack struct
      typedef struct _pathStack{
         char items[50];
         int size;
      }pathStack;

//#################################################################
//BEGINNING OF STATIC FUNCTION DECLARATIONS
//#################################################################
   //pathStack:
      static char top(PathStack s);
      static int size(PathStack s);
      static void push(PathStack s,char c);
      static char pop(PathStack s);
      //transfers contents of PathStack to newPath
      static void transfer(PathStack s,char* newPath);
      static void initialize(PathStack s,char* initial);
      
   //mode:
      static nodePtr newNetwork(Game g);
      static nodePtr goTo(nodePtr origin,char* path,int* lastDir);
      static void allNodePaths(nodePtr origin,path* allPaths);

   //tools and weapons
      static int isEmptyPaths(path* paths);
      static int oppositeDir(int i);
      static void ARCpath(nodePtr origin,path ret,path from,path to);
      static action newAction(int code,path destination,int disciplineFrom,int disciplineTo);
#ifdef MAIN_GAME
   int main(int argc, char *argv[]) {
      // miscellaneous
      /*int disciplines[NUM_REGIONS];
      int dice[NUM_REGIONS];*/
      Game g;
      
      // store the winner of each game
      int winner;
      
      // store game states within the game
      int keepPlaying;
      int turnFinished;
      int diceRollAmount;
      
      // random
      char *actions[] = ACTION_NAMES;
      int diceRoll;
      int turnPerson;
      
      int passedTurns = 0;
               
      // seed rand!
      srand(time(NULL));
      
      // while the game is wanting to be played, create new game, etc.
      keepPlaying = TRUE;
      while (keepPlaying == TRUE) {
         // create the game
         //randomDisciplines(disciplines);
         //randomDice(dice);
         
         // you can change this to randomiseDisciplines() and randomiseDice() later
         int disciplines[NUM_REGIONS] = {CYAN,PURP,YELL,PURP,YELL,RED ,GREE,GREE, RED ,GREE,CYAN,YELL,CYAN,BLUE,YELL,PURP,GREE,CYAN,RED };
         int dice[NUM_REGIONS] = {9,10,8,12,6,5,3,7,3,11,4,6,4,9,9,2,8,10,5};

         // rig board like the real game
         rigBoard(disciplines, dice);
         g = newGame(disciplines, dice);
         
         printf("Game created! Now playing...\n");
         
         // start the game with noone as the winner
         winner = NO_ONE;
         while (winner == NO_ONE) {
            
            // start new turn by setting turnFinished to false then
            // rolling the dice
            
            diceRollAmount = 0;
            diceRoll = 0;
            while (diceRollAmount < DICE_AMOUNT) {
               diceRoll += rollDice();
               diceRollAmount++;
            }
            
            throwDice(g, diceRoll);
         
            
            // keep going through the player's turn until
            // he/she decided to pass and finish the turn
            turnFinished = FALSE;
            while (turnFinished == FALSE && passedTurns < MAX_PASS) {
               
               turnPerson = getWhoseTurn(g);
               // processes requests and all subrequests for a move and
               // checks if they are legal. only gives a move within the
               // scope of the defined actionCodes that is legal
               
               action a = decideAction(g);
               system("clear");
               printBoard(g);
               printf("ACTION: %s\n",actions[a.actionCode]);
               printf("TURN: %5d\n",getTurnNumber(g));
               printf("DICE: %5d\n",diceRoll);
               // if not passing, make the move; otherwise end the turn
               if (a.actionCode == PASS) {
                  turnFinished = TRUE;
                  //printf("You have passed onto the next person.\n");
               } else {

                  assert(isLegalAction(g, a));                                   
               
                  // break this and the code dies. trololol!
                  if (a.actionCode == START_SPINOFF) {
                     if (rand() % 3 <= 1) {
                        a.actionCode = OBTAIN_PUBLICATION;
                     } else {
                        a.actionCode = OBTAIN_IP_PATENT;               
                     }
                  }
                  makeAction(g, a);
                  if (a.actionCode == PASS) {
                     passedTurns++;
                  } else {
                     passedTurns = 0;
                  }
                  
                  if (passedTurns >= MAX_PASS || getKPIpoints(g, turnPerson) >= WINNING_KPI) {
                     turnFinished = TRUE;
                     
                  }
               }
               
               // if there is not a winner or pass, add a seperating line
               // to seperate actions being clumped together
            }
            
            // check if there is a winner
            winner = checkForWinner(g);
         }
         
         if (passedTurns >= MAX_PASS) {
            printf("AI passes too much.\n");
            return EXIT_FAILURE;
         }
         
         printLineBreak();
         printf("GAME OVER!\n");
         printf("Vice Chanceller %c Won in %d Turns!!\n", 
                winner + UNI_CHAR_NAME,
                getTurnNumber(g));
                
         printf("\n");
         int counter = UNI_A;
         while (counter < NUM_UNIS + UNI_A) {
            printf("Uni %c scored %d KPIs\n", counter + UNI_CHAR_NAME,
                   getKPIpoints(g, counter));
            counter++;
         }
         printLineBreak();
         
         disposeGame(g);
         
         // ask to play again
         printf("Ctrl+C will exit the game.\nOtherwise, the game will "
                "recommence by hitting enter.");
         int a = scanf("%*c");
         a++;
      }     
      
      return EXIT_SUCCESS;
   }
#endif

#ifdef MAIN_TEST
   int main(int argc, char const *argv[])
   {
      int disciplines[NUM_REGIONS] = {CYAN,PURP,YELL,PURP,YELL,RED ,GREE,GREE, RED ,GREE,CYAN,YELL,CYAN,BLUE,YELL,PURP,GREE,CYAN,RED };
      int dice[NUM_REGIONS] = {9,10,8,12,6,5,3,7,3,11,4,6,4,9,9,2,8,10,5};
      Game g = newGame(disciplines,dice);
      throwDice(g,2);
      action a = newAction(OBTAIN_ARC,"R",0,0);
      makeAction(g,a);
      printBoard(g);

      disposeGame(g);
      return 0;
   }
#endif
// ----- game creation -----

   void rigBoard(int disciplines[], int dice[]) {
     disciplines[0] = disciplines[2] = disciplines[7] = STUDENT_BPS;
     disciplines[11] = disciplines[16] = disciplines[18] = STUDENT_BQN;
   }

   // Allocates a set of random disciplines inside disciplines[]
   void randomDisciplines(int disciplines[]) {
      int disciplineIndex;
      
      disciplineIndex = 0;
      while (disciplineIndex < NUM_REGIONS) {
         // allocate each discipline with a random one
         disciplines[disciplineIndex] = rand() % NUM_DISCIPLINES;
         disciplineIndex++;
      }
   }

   // Allocates a set of random dice inside disciplines[]
   void randomDice(int dice[]) {
      int diceIndex;
      int diceRolled;
      int totalRoll;
      
      diceIndex = 0;
      while (diceIndex < NUM_REGIONS) {
         totalRoll = 0;

         // roll a dice DICE_AMOUNT and add the total
         diceRolled = 0;
         while (diceRolled < DICE_AMOUNT) {
            totalRoll += rollDice();
            diceRolled++;
         }
         
         // allocate the totalRoll
         dice[diceIndex] = totalRoll;
         diceIndex++;
      }
   }

   // return a number between 1...DICE_FACES 
   int rollDice(void) {
      // modding returns between 0...(DICE_FACES-1), so add 1
      return (rand() % DICE_FACES) + 1;
   }

   // ----- game actions -----

   // check all players' KPI and returns the winner (if any)
   int checkForWinner(Game g) {
      int winner = NO_ONE;
      int playerIndex;
      
      playerIndex = UNI_A;
      while (playerIndex < NUM_UNIS + UNI_A) {
         // check if the player is over or equal the WINNING_KPI
         if (getKPIpoints(g, playerIndex) >= WINNING_KPI) {
            winner = playerIndex;
         }
         playerIndex++;
      }
      
      return winner;
   }

   // prints a new line, line break, then new line again
   void printLineBreak(void) {
      int counter;
      
      printf("\n");
      
      // print the line break (SCREEN_WIDTH amount of 
      // LINE_BREAK_SEPARATOR)
      counter = 0;
      while (counter < SCREEN_WIDTH) {
         printf("%c", LINE_BREAK_SEPARATOR);
         counter++;
      }
      
      printf("\n\n");
   }

//#################################################################
//Network and friends
//#################################################################
   //pathStack
      static void initialize(PathStack s,char* initial){
         s->size = strlen(initial);
         strcpy(s->items,initial);
         
      }

      static void transfer(PathStack s,char* newPath){
         strcpy(newPath,s->items);
      }

      static char top(PathStack s){
          char ret;
          if(s->size > 0)
          ret = s->items[(s->size) - 1];
            else ret = '\0';
            return ret;
      }

      static int size(PathStack s){
          return (s->size);
      }

      static void push(PathStack s, char c){
          s->items[(s->size)]=c;
          (s->size)++;
          s->items[s->size] = '\0';
      }

      static char pop(PathStack s){
            assert(s->size != 0);
            char ret = top(s);
          s->items[(s->size)-1]='\0';
          (s->size)--;
          return ret;
      }

   //node
      static nodePtr newNetwork(Game g){
         int i;
         nodePtr vertexTmp = NULL,ret = NULL;
         int linkType;

         int trainingCentres[] = TRAINING_CENTRES;
         int trainingDisciplines[] = TRAINING_DISCIPLINES;

         ret = malloc(sizeof(struct _node)*NUM_NODES);
         assert(ret != NULL);


         //following while sets all the values of Origin array to default
         //and creates a network with *next
            vertexTmp = ret;
            i = 0;
            while(i<NUM_NODES) {
               vertexTmp->nodeID = i;
               vertexTmp->training = NO_TRAINING;
               vertexTmp->neighbour[0] = NULL;
               vertexTmp->neighbour[1] = NULL;
               vertexTmp->neighbour[2] = NULL;
               vertexTmp->neighbour[3] = NULL;
               if(i<NUM_NODES-1) {
                  vertexTmp = vertexTmp;
                  vertexTmp++;
                  vertexTmp->next = vertexTmp;
               }
               else{
                  vertexTmp->next = NULL;
               }
               i++;
            }

         //Initializing all links (neighbours)
            vertexTmp = ret;
            //set links between vertices 0-5 48-53;
            //RIGHT LEFT
            i = 0;
            while(i<3) {
               vertexTmp[2*i].neighbour[RIGHT] = &vertexTmp[2*i+1];
               vertexTmp[48+ 2*i].neighbour[RIGHT] = &vertexTmp[48+ 2*i +1];
               vertexTmp[2*i+1].neighbour[LEFT] = &vertexTmp[2*i];
               vertexTmp[48+ 2*i+1].neighbour[LEFT] = &vertexTmp[48+ 2*i];
               i++;
            }

            i = 0;
            //UP DOWN
            while(i<2) {
               vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+3];
               vertexTmp[i+3].neighbour[UP] = &vertexTmp[i];
               
               vertexTmp[49+ i].neighbour[DOWN] = &vertexTmp[49+i+3];
               vertexTmp[49+i+3].neighbour[UP] = &vertexTmp[49+i];
               i++;
            }

            //sets links between 3rd - 2nd and 9th - 10th
            i = 0;
            while(i<4) {
               vertexTmp[2+i].neighbour[DOWN] = &vertexTmp[7+i];

               vertexTmp[7+i].neighbour[UP] = &vertexTmp[2+i];

               vertexTmp[43+i].neighbour[DOWN] = &vertexTmp[48+i];
               vertexTmp[48+i].neighbour[UP] = &vertexTmp[43+i];
               i++;
            }

            //sets links within 3rd
            i = 0;
            while(i<3) {
               vertexTmp[6+ 2*i].neighbour[RIGHT] = &vertexTmp[6 + 2*i +1];
               vertexTmp[6+ 2*i +1].neighbour[LEFT] = &vertexTmp[6 + 2*i];

               vertexTmp[42+ 2*i].neighbour[RIGHT] = &vertexTmp[42+ 2*i +1];
               vertexTmp[42+ 2*i +1].neighbour[LEFT] = &vertexTmp[42 + 2*i];
               i++;
            }

            //sets relation of 3rd and 9th with others
            i = 6;
            while(i<12) {
               vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
               vertexTmp[i+36].neighbour[UP] = &vertexTmp[i-6+36];
               i++;
            }

            //sets links of all others only UP DOWN
            i = 12;
            while(i<42) {
               vertexTmp[i].neighbour[UP] = &vertexTmp[i-6];
               vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
               i++;
            }


            i = 12;
            linkType = ODD_EVEN;
            while(i<42) {
               if(linkType == ODD_EVEN) {
                  if(i%2!=0) {
                     vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
                     vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
                  }
               }
               else {
                  if(i%2==0) {
                     vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
                     vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
                  }
               }
               i++;
               if(i%6==0) {
                  linkType = !linkType;
               }
            }

         //some extra link breaking as the above while creates extra links
         //namely 17-18, 29-30 41-42 these links extend over the boundary
         //i.e right edge connecting to left edge of board

            vertexTmp = ret;
            vertexTmp[17].neighbour[RIGHT]=NULL;
            vertexTmp[29].neighbour[RIGHT]=NULL;
            vertexTmp[41].neighbour[RIGHT]=NULL;

            vertexTmp[18].neighbour[LEFT]=NULL;
            vertexTmp[30].neighbour[LEFT]=NULL;
            vertexTmp[42].neighbour[LEFT]=NULL;
      
         //intiailizing training centres
            i = 0;
            while(i<MAX_RETRAIN_CENTRES) {
               ret[trainingCentres[i]].training = trainingDisciplines[i];
               
               i++;
            }  
         
         //Final Return
            return ret;
      }

      static void allNodePaths(nodePtr origin,path* allPaths){
         pathStack p;
         PathStack paths = &p;
         nodePtr nTmp = NULL;
         initialize(paths,"");
         assert(paths!=NULL);
         path newPath;
         char tmp;
         int i;
         int state = START;

         //intiailizes allPaths to empty strings
         i=0;
         while(i<NUM_NODES){
            strcpy(allPaths[i],"RRRRR RRRRR RRR");
            i++;
         }

         while(isEmptyPaths(allPaths)==TRUE){
            transfer(paths,newPath);
            
            nTmp = goTo(origin,newPath,NULL);
            //printf("%d\n",nodeID);
            if(nTmp != NULL){
               #ifdef LOG_ALLPATHS
                  if(nTmp->nodeID == 42)
                  printf("ID:%d old %d new %d\n",nTmp->nodeID,(int)strlen(allPaths[nTmp->nodeID]),(int)strlen(newPath));
               #endif
               if(strlen(allPaths[nTmp->nodeID])>strlen(newPath))
               strcpy(allPaths[nTmp->nodeID],newPath);
            }

            if(nTmp == NULL){
               tmp = top(paths);
               if(tmp=='L'){
                  pop(paths);
                  push(paths,'R');
               }
               else{
                  while(top(paths)=='R'){
                     pop(paths);
                  }
                  if(size(paths)!=0){
                     pop(paths);
                     push(paths,'R');
                  }
               }
            }

            else if(size(paths)==0){
               if(state == START){
                  push(paths,'L');
                  state = FORWARD;
               }
               else {
                  state = EXIT;
               }
                  
            }
            else if(size(paths)>=MAX_POSSIBLE_PATH){
               tmp = top(paths);
               if(tmp=='L'){
                  pop(paths);
                  push(paths,'R');
               }
               else{
                  pop(paths);
                  while(top(paths)=='R'){
                     pop(paths);
                  }
                  if(size(paths)>0){
                     pop(paths);
                     push(paths,'R');
                  }
               }
            }
            else{
               push(paths,'L');
            }
            #ifdef LOG_ALLPATHS
               path t;
               transfer(paths,t);
               printf("%s\n",t);
            #endif
         }     
      }
   
      static nodePtr goTo(nodePtr origin,char* path,int* lastDir) {
         
         nodePtr traveller = NULL;
         char* pathDir = path;
         int right = FALSE,it = 0,last=DOWN;
         int tmp,state = VALUE_FOUND;

         int dir1,dir2,dir3;

         traveller = origin;
         if(!(*pathDir == 'R' || *pathDir == 'L' ||*pathDir == '\0')){
            traveller = NULL;
            state = VALUE_NOTFOUND;
         }

         //the first 'shift' is done at origin, 
         //this is straightforward as the relative right 'R' is the left neighbour when you look form above
         if(*pathDir == 'R') {
            //the direction of neighbour is opp of the pathDir
            traveller = traveller->neighbour[DOWN];
            last = DOWN;
            pathDir++;
         }
         else if(*pathDir == 'L'){
            traveller = traveller->neighbour[RIGHT];
            last = RIGHT;
            pathDir++;  
         }

         

         tmp = -1;

         //after the first shift it has to enter a while to 
         //get through the remaining path 
         while(*pathDir!='\0' && state == VALUE_FOUND) {
            state = VALUE_NOTFOUND;
            assert(*pathDir == 'R' || *pathDir == 'L' ||*pathDir == 'B' );
            //it is iterator and it will start from the opposite of last
            //as last was relative to the previous vertex 
            it = oppositeDir(last);
            right = FALSE;
            if(*pathDir == 'B') {
               tmp = oppositeDir(last);
               state = VALUE_FOUND;
            }
            else{
               it = (it+1)%4;
               if(traveller->neighbour[it]!=NULL){
                  right = TRUE;
                  dir1 = TRUE;
                  if(*pathDir == 'R'){
                     tmp = it;
                     state = VALUE_FOUND;
                  }
               }
               it = (it+1)%4;
               if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
                  dir2= TRUE;
                  if(right == TRUE){
                     //left
                     if(*pathDir == 'L'){
                        tmp = it;
                        state = VALUE_FOUND;
                     }
                  }
                  else {
                     right = TRUE;
                     if(*pathDir == 'R'){
                        tmp = it;
                        state = VALUE_FOUND;
                     }
                  }
               }
               it = (it+1)%4;
               if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
                  //left
                  dir3 = TRUE;
                  if(*pathDir == 'L'){
                     tmp = it;
                     state = VALUE_FOUND;
                  }
               }
               it = oppositeDir(last);
               dir1 = dir2 = dir3 = FALSE;
               if(traveller->neighbour[(it+1)%4]!=NULL)dir1 = TRUE;
               if(traveller->neighbour[(it+2)%4]!=NULL)dir2 = TRUE;
               if(traveller->neighbour[(it+3)%4]!=NULL)dir3 = TRUE;
               if(dir1 == FALSE && dir2 == TRUE && dir3==FALSE){
                  if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+3)%4]!=NULL){
                     if(*pathDir == 'R'){
                        tmp = last;
                        state = VALUE_FOUND;
                     }
                     else {
                        state = VALUE_NOTFOUND;
                     }
                  }
                  else if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+1)%4]!=NULL){
                     if(*pathDir == 'L'){
                        tmp = last;
                        state = VALUE_FOUND;
                     }
                     else {
                        state =VALUE_NOTFOUND;
                     }  
                  }
                  else {
                     state = VALUE_NOTFOUND;
                  }
               }
            }

            if(state == VALUE_FOUND){
               traveller = traveller->neighbour[tmp];
               last = tmp;
               pathDir++;
            }
            else{
               traveller = NULL;
            }
         }
         if(state == VALUE_NOTFOUND){
            traveller = NULL;
         }

         if(lastDir != NULL) {
            *lastDir = last;
         }

         return traveller;
      }

   //tools and weapons
      static int isEmptyPaths(path* paths){
         int i=1;
         int ret = FALSE;
         while(i<NUM_NODES && ret == FALSE){
            if(strlen(paths[i])>MAX_POSSIBLE_PATH){
               ret = TRUE;
            }
            i++;
         }
         return ret;
      }

      static int oppositeDir(int i) {
         assert(i>=0&&i<4);
         int ret;
         ret = i+2;
         if(ret>=4) {
            ret-=4;
         }

         return ret;
      }

      static void ARCpath(nodePtr origin,path ret,path from,path to){
         int last;
         nodePtr start = goTo(origin,from,&last);
         assert(start != NULL);
         int i,j,size = strlen(from);
         int right;
         nodePtr end = goTo(origin,to,NULL);
         path tmp;
         strcpy(tmp,from);
         strcpy(ret,"");

         i= UP;
         while(start->neighbour[i]!= end && i<RIGHT){
            i++;
         }
         if(start->neighbour[i] == end){

            
            right = FALSE;
            j= oppositeDir(last);
            if(j==i){
               strcpy(ret,from);
               ret[size] = 'B';
               ret[size+1] = '\0';
            }
            else{
               j= (j+1)%4;
               if(start->neighbour[j]!=NULL){
                  right = TRUE;
                  if(j==i){
                     strcpy(ret,from);
                     ret[size] = 'R';
                     ret[size+1] = '\0';
                  }
               }
               j = (j+1)%4;
               if(start->neighbour[j]!=NULL){
                  if(right == TRUE){
                     if(j==i){
                        strcpy(ret,from);
                        ret[size] = 'L';
                        ret[size+1] = '\0';
                     }
                  }
                  else{
                     right = TRUE;
                     if(j==i){
                        strcpy(ret,from);
                        ret[size] = 'R';
                        ret[size+1] = '\0';
                     }
                  }
               }
               j=(j+1)%4;
               if(start->neighbour[j]!=NULL){
                  if(j==i){
                     strcpy(ret,from);
                     ret[size] = 'L';
                     ret[size+1] = '\0';
                  }
               }
            }
         }
      }

      static action newAction(int code,path destination,int disciplineFrom,int disciplineTo){
         action ret;
         ret.actionCode = code;
         strcpy(ret.destination,destination);
         ret.disciplineFrom = disciplineFrom;
         ret.disciplineTo = disciplineTo;

         return ret;
      }

//#################################################################
//Printers
//#################################################################

   void printBoard(Game g){
      int i,j,k,l,area;
      
      i=0;
      char representARC[] = {REP_VAC,REP_A,REP_B,REP_C};
      char representUNI[] = {' ','A','B','C'};
      path arc,node;
      nodePtr cursor = NULL;
      //initializing grid and values
         int grid[GRID_ROW][GRID_COL]={
            {NONE,NONE,NONE,NONE,NONE,NODE,NODE,NONE,NONE,NONE,NONE,NONE},
            {NONE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NONE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},

            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NONE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NONE},
            {NONE,NONE,NONE,NONE,NONE,NODE,NODE,NONE,NONE,NONE,NONE,NONE}          
         };
         int values[GRID_ROW][GRID_COL]={
            {NONE,NONE,NONE,NONE,NONE,NODE,NODE,NONE,NONE,NONE,NONE,NONE},
            {NONE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NONE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},

            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE},
            {NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE},
            {NONE,NONE,NONE,NODE,NODE,NONE,NONE,NODE,NODE,NONE,NONE,NONE},
            {NONE,NONE,NONE,NONE,NONE,NODE,NODE,NONE,NONE,NONE,NONE,NONE}            
         };
         //initializing disciplines and dice
            j =1;k=0;l=0;
            while(j<GRID_COL-1){
               i=1;
               while(i<GRID_ROW-1){
                  if(grid[i][j-1] == NODE && grid[i+1][j] == NODE && grid[i-1][j] == NODE && grid[i][j] == NONE){
                     values[i][j] = getDiscipline(g,k);
                     grid[i][j] = DISC;
                     k++;
                  }
                  if(grid[i][j+1] == NODE && grid[i+1][j] == NODE && grid[i-1][j] == NODE && grid[i][j] == NONE){
                     values[i][j] = getDiceValue(g,l);
                     grid[i][j] = DICE;
                     l++;
                  }
                  i++;
               }
               j++;
            }
         //initializing node values
            i =0;k=0;l=0;
            while(i<GRID_ROW){
               j=0;
               while(j<GRID_COL){
                  if(grid[i][j] == NODE){
                     values[i][j] = k;
                     k++;
                  }
                  j++;
               }
               i++;
            }
      //initializing the paths and origin
         nodePtr origin = newNetwork(g);
         path allPaths[54];
         allNodePaths(origin,allPaths);
      
      area = UPPER;
      i=0;
      //*
      while(i<GRID_ROW){
         j=0;
         while(j<GRID_COL){
            if(grid[i][j]==NODE){

               cursor = &origin[values[i][j]];
               if(area == UPPER){
                  if(cursor->neighbour[UP]!=NULL){
                     ARCpath(origin,arc,allPaths[cursor->nodeID],allPaths[cursor->neighbour[UP]->nodeID]);
                     if(cursor->neighbour[RIGHT]!=NULL){
                        printf("%c  ",representARC[getARC(g,arc)]);
                     }
                     else if(cursor->neighbour[LEFT]!=NULL){
                        printf("  %c",representARC[getARC(g,arc)]);

                     }
                     else{
                        if(cursor->neighbour[UP]->neighbour[RIGHT]!=NULL){
                           printf("  %c",representARC[getARC(g,arc)]);
                        }
                        else if(cursor->neighbour[UP]->neighbour[LEFT]!=NULL){
                           printf("%c  ",representARC[getARC(g,arc)]);
                        }
                     }
                  }
                  else printf("   ");
               }
               else if(area==MIDDLE){
                  if(cursor->neighbour[LEFT]!=NULL){
                     ARCpath(origin,arc,allPaths[cursor->nodeID],allPaths[cursor->neighbour[LEFT]->nodeID]);
                     printf("%c",representARC[getARC(g,arc)]);
                  }
                  else {printf(" ");}
                  strcpy(node,allPaths[cursor->nodeID]);
                  printf("%c",representUNI[getCampus(g,node)]);
                  if(cursor->neighbour[RIGHT]!=NULL){
                     ARCpath(origin,arc,allPaths[cursor->nodeID],allPaths[cursor->neighbour[RIGHT]->nodeID]);
                     printf("%c",representARC[getARC(g,arc)]);
                     
                  }
                  else {printf(" ");}
               }
               else if(area == LOWER){
                  if(cursor->neighbour[DOWN]!=NULL){
                     ARCpath(origin,arc,allPaths[cursor->nodeID],allPaths[cursor->neighbour[DOWN]->nodeID]);
                     if(cursor->neighbour[RIGHT]!=NULL){
                        printf("%c  ",representARC[getARC(g,arc)]);
                     }
                     else if(cursor->neighbour[LEFT]!=NULL){
                        printf("  %c",representARC[getARC(g,arc)]);

                     }
                     else{
                        if(cursor->neighbour[DOWN]->neighbour[RIGHT]!=NULL){
                           printf("  %c",representARC[getARC(g,arc)]);
                        }
                        else if(cursor->neighbour[DOWN]->neighbour[LEFT]!=NULL){
                           printf("%c  ",representARC[getARC(g,arc)]);
                        }
                     }
                  }
                  else printf("   ");
               }
            }
            else if(grid[i][j]==DISC){
               if(area == UPPER){
                  printf("   " );
               }
               else if(area == LOWER){
                  printf("   ");
               }
               else if(area == MIDDLE){
                  printf("%2d ",values[i][j]);
               }
            }
            else if(grid[i][j]==DICE){
               if(area == UPPER){
                  printf("   " );
               }
               else if(area == LOWER){
                  printf("   ");
               }
               else if(area == MIDDLE){
                  printf("%2d ",values[i][j]);
               }
            }
            else if(grid[i][j] == NONE){
               printf("   ");
            }
            j++;  
         }
         area = (area+1)%3;
         printf("\n");
         if(area==UPPER)
         {i++;}
      }
      //*/


      printf("|Player|KPI| THD|BPS|BQN| MJ|MTV|MMONEY|  ARCS|CAMPUS|  IP| PUB|REP|TURN|\n");
      i = UNI_A;
      while(i<=UNI_C){
         printf("|%6c|",representUNI[i]);
         printf("%3d|",getKPIpoints(g,i));
         printf("%4d|",getStudents(g,i,0));
         j=1;
         while(j<5){
            printf("%3d|",getStudents(g,i,j));
            j++;
         }
         printf("%6d|", getStudents(g,i,j));
         printf("%6d|",getARCs(g,i));
         printf("%6d|",getCampuses(g,i));
         printf("%4d|",getIPs(g,i));
         printf("%4d|",getPublications(g,i));
         printf("%3c|",representARC[i]);
         if(getWhoseTurn(g) == i){printf(" <  |");}
         else {printf("    |");}
         i++;
         printf("\n");
      }

      i=0;
      while(i<EXCESS_LINES){
         printf("\n");
         i++;
      }
      /*
      i=0;
      while(i<GRID_ROW){
         j=0;
         while(j<GRID_COL){

            printf("%2d ", grid[i][j]);
            j++;
         }
         printf("\n");
         i++;
      }
      //*/
      free(origin);
   }