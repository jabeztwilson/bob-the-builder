/*
 *Jabez Wilson
 *Sijia Chen
 *
 */

#include "Game.h"
#include "mechanicalTurk.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>


//defines for struct definition
 #define DIRECTIONS 4
 #define AREAS_PER_NODE 3
 #define NUM_NODES 54
 #define NUM_RESOURCES 6
 #define NUM_NODES_PER_AREA 6
 #define NUM_REGIONS 19

//directions
	#define UP 0
	#define LEFT 1
	#define DOWN 2
	#define RIGHT 3

//other important MACROS
	#define EVEN_ODD 1
	#define ODD_EVEN 0

	#define VALUE_FOUND 1
	#define VALUE_NOTFOUND 0

 	#define MAX_POSSIBLE_PATH 11

//VALUES for board
 	#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
					STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
					STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
					STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
					STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
	#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

	#define DISCIPLINES_BPS {STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,\
					STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,\
					STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,\
					STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,STUDENT_BPS,\
					STUDENT_BPS,STUDENT_BPS,STUDENT_BPS}
	#define DICE_4 {4,4,4, 4,4,4,4, 4,4,4,4,4, 4,4,4,4, 4,4,4}

	#define TRAINING_CENTRES {2,3,4,5,29,35,46,51,48,43}
	#define TRAINING_DISCIPLINES {STUDENT_MTV,STUDENT_MTV,\
															STUDENT_MMONEY,STUDENT_MMONEY,\
														  STUDENT_BQN,STUDENT_BQN,\
														  STUDENT_MJ,STUDENT_MJ,\
														  STUDENT_BPS,STUDENT_BPS}
	#define MAX_RETRAIN_CENTRES 10
	#define NO_TRAINING 6
	#define START_OF_REGIONS {6,18,30,2,13,25,37,0,8,20,32,44,4,15,27,39,10,22,34}
	#define DIR_AROUND_HEX {RIGHT,DOWN,DOWN,LEFT,UP} 

//SCORES
	#define SCORE_BASE 10
	#define SCORE_THD (-SCORE_BASE*SCORE_BASE)				//-1000
	#define SCORE_BPS SCORE_BASE		// 1000
	#define SCORE_BQN SCORE_BASE		// 1000
	#define SCORE_MJ SCORE_BASE					// 0010
	#define SCORE_MTV SCORE_BASE				// 0010
	#define SCORE_MMONEY (SCORE_BASE/2)				// 0010
	#define SCORE_DICE7 (-1*SCORE_BASE/2)		// 0010
	#define SCORE_MAX (SCORE_BASE*SCORE_BASE +SCORE_BASE)	//0110
	#define PASS_SCORE 0
	#define PASS_ARCSCORE 0

//MACROS for path states
	#define BACK 2
	#define START 3
	#define FORWARD 4
	#define EXIT 9

//MACROS for printNetwork	
	#define GRID_ROW 11
	#define GRID_COL 6			
	#define UPPER 0
	#define MIDDLE 1
	#define LOWER 2		
	#define EMPTY_V '|'
	#define EMPTY_H	'-'

//TEST MACROS
	//#define TEST_ARSENAL
	//#define TEST_STACK
	//#define TEST_NETWORK
	//#define TEST_VALUES

//MAIN MACROS
	//#define MAIN_TEST
	//#define MAIN_PRINT

//LOGs
	//#define LOG_GOTO
	//#define LOG_ALLPATHS
	//#define LOG_VALUENODE
	//#define LOG_VALUABLENODE

//PRINT MACROS
	//#define PRINT_BOARD
	//#define PRINT_ALLPATHS

//Random
	#define START_RAND 20
	#define RANGE_RAND 120
	#define POSSIBLE 0
	#define ONLY_PASS 1

//DEBUG MACROS
	#define MARK printf("marked\n");

//NOT_SUBMITTING
	//#define NOT_SUBMITTING


//#################################################################
//BEGINNING OF STRUCT DEFINITIONS
//#################################################################
	typedef struct _node *nodePtr;
	typedef struct _node node;

	typedef struct _pathStack * PathStack;

	//node struct
		struct _node {
		 	//node Id of this node
		 	int nodeID;
			//next pointers to neghboring vertices
		 	nodePtr next, neighbour[DIRECTIONS];
		 	int training;
		 	int campus,ARC[DIRECTIONS];
		};
	//path stack struct
		typedef struct _pathStack{
			char items[50];
			int size;
		}pathStack;

//#################################################################
//BEGINNING OF STATIC FUNCTION DECLARATIONS
//#################################################################
	//pathStack:
		static char top(PathStack s);
		static int size(PathStack s);
		static void push(PathStack s,char c);
		static char pop(PathStack s);
		//transfers contents of PathStack to newPath
		static void transfer(PathStack s,char* newPath);
		static void initialize(PathStack s,char* initial);
		
	//mode:
		static nodePtr newNetwork(Game g);
		static nodePtr goTo(nodePtr origin,char* path,int* lastDir);
		static void allNodePaths(nodePtr origin,path* allPaths);
		
	//tools and weapons
		static int isEmptyPaths(path* paths);
		static int oppositeDir(int i);
		static void ARCpath(nodePtr origin,path ret,path from,path to);
		static action newAction(int code,path destination,int disciplineFrom,int disciplineTo);

	//random
		static action randomCampus(Game g,nodePtr origin);
		static action randomARC(Game g,nodePtr origin);
		static action randomGO8(Game g,nodePtr origin);

	#ifdef NOT_SUBMITTING
		//tests
			static void testOppositeDir();
			static void testShortestDist();
			static void testARCpath();
			static void testNewAction();

	 		static void testGoTo();
	 		static void testAllNodePaths();
	 		static void testNewNetwork();

	 		static void testStack();

	 		static void testValueNode();
	 		static void testValueARC();
	 		static void testValuableNode();
	 		static void testValuableARC();

	 	//printf
	 		static void printNetwork(nodePtr origin);
	#endif

//#################################################################
//Decide Action
//#################################################################
	action decideAction(Game g){

		action ret;
		action allActions[8];
		int i,selector;
		int min,max,from,to;
		int tmp,playerID = getWhoseTurn(g);
		int status = ONLY_PASS;
		srand(time(NULL));
		nodePtr origin = newNetwork(g);
		i=0;
		while(i<8){
			allActions[i] = newAction(PASS,"",0,0);
			i++;
		}

		allActions[BUILD_CAMPUS] = randomCampus(g,origin);
		allActions[OBTAIN_ARC] = randomARC(g,origin);
		allActions[BUILD_GO8] = randomGO8(g,origin);
		allActions[START_SPINOFF] = newAction(START_SPINOFF,"",0,0);
		if(isLegalAction(g,allActions[START_SPINOFF])==FALSE){
			allActions[START_SPINOFF] = newAction(PASS,"",0,0);
		}

		i=0;
		while(i<8){
			if(allActions[i].actionCode != PASS)status = POSSIBLE;
			i++;
		}
		if(status == ONLY_PASS){
			ret = allActions[0];
		}
		else{
			selector = rand()%8;
			while(allActions[selector].actionCode == PASS){selector = rand()%8;}
			ret = allActions[selector];
		}
		if(ret.actionCode == PASS) {
			i=1;max = 0;min = 25;
			from = 5;
			to = 1;
			//status = VALUE_NOTFOUND;
			ret = newAction(PASS,"",1,1);
			while(i<6){
				tmp = getStudents(g,playerID,i);
				if(tmp>max){
					max = tmp;
					from = i;
				}
				i++;
			}
			i=1;
			while(i<6){
				tmp = getStudents(g,playerID,i);
				if(tmp<min){
					min = tmp;
					to = i;
				}
				i++;
			}
			if(max > 2){
				ret = newAction(RETRAIN_STUDENTS,"",from,to);
			}
		}

		free(origin);
		assert(isLegalAction(g,ret)==TRUE);
		return ret;
	}

//#################################################################
//BEGINNING OF MAIN
//#################################################################

	#ifdef NOT_SUBMITTING
		#ifdef MAIN_TEST
		 	int main(int argc, char const *argv[])
		 	{
		 		#ifdef TEST_ARSENAL
		 			testARCpath();
		 			testOppositeDir();
		 			testShortestDist();
		 			testNewAction();
		 			printf("ARSENAL TESTS PASSED!\n");
		 		#endif

		 		#ifdef TEST_NETWORK
		 			testGoTo();
		 			//testAllNodePaths();
		 			printf("NETWORK TESTS PASSED!\n");
		 		#endif

		 		#ifdef TEST_STACK
		 			testStack();
		 			printf("STACK TESTS PASSED\n");
		 		#endif

		 		#ifdef TEST_VALUES
		 			testValueNode();
		 			testValueARC();
		 			testValuableNode();
		 			testValuableARC();
		 			printf("VALUE TESTS PASSED\n");
		 		#endif

		 		return EXIT_SUCCESS;
		 	}
		#endif

		#ifdef MAIN_PRINT
		 	int main(int argc, char const *argv[])
		 	{
		 		int i;
		 		int disciplines[] = DEFAULT_DISCIPLINES;
				int dice[] = DEFAULT_DICE;
				Game g = newGame(disciplines,dice);
				nodePtr origin = newNetwork(g);
				path allPaths[NUM_NODES];

				#ifdef PRINT_BOARD
		 			printNetwork(origin);
		 		#endif

		 		
		 		#ifdef PRINT_ALLPATHS
		 			allNodePaths(origin,allPaths);
		 			i=0;
		 			while(i<NUM_NODES){
		 				printf("%d %s\n",i,allPaths[i]);
		 				i++;
		 			}
		 		#endif

		 		disposeGame(g);
		 		
		 		free(origin);
		 		return 0;
		 	}
		#endif
	#endif

//#################################################################
//JABEZ'S FUNCTIONS
//#################################################################
	//pathStack
		static void initialize(PathStack s,char* initial){
			s->size = strlen(initial);
			strcpy(s->items,initial);
			
		}

		static void transfer(PathStack s,char* newPath){
			strcpy(newPath,s->items);
		}

	//node
		static nodePtr newNetwork(Game g){
			int i,j;
			nodePtr vertexTmp = NULL,ret = NULL;
			int linkType;

			path allPaths[NUM_NODES];
			path arc;

			int trainingCentres[] = TRAINING_CENTRES;
			int trainingDisciplines[] = TRAINING_DISCIPLINES;
			
			ret = malloc(sizeof(struct _node)*NUM_NODES);
			assert(ret != NULL);


			//following while sets all the values of Origin array to default
			//and creates a network with *next
				vertexTmp = ret;
				i = 0;
				while(i<NUM_NODES) {
					vertexTmp->nodeID = i;
					vertexTmp->training = NO_TRAINING;
					vertexTmp->neighbour[0] = NULL;
					vertexTmp->neighbour[1] = NULL;
					vertexTmp->neighbour[2] = NULL;
					vertexTmp->neighbour[3] = NULL;
					vertexTmp->ARC[0] = VACANT_ARC;
					vertexTmp->ARC[1] = VACANT_ARC;
					vertexTmp->ARC[2] = VACANT_ARC;
					vertexTmp->ARC[3] = VACANT_ARC;
					vertexTmp->campus = VACANT_VERTEX;
					if(i<NUM_NODES-1) {
						vertexTmp = vertexTmp;
						vertexTmp++;
						vertexTmp->next = vertexTmp;
					}
					else{
						vertexTmp->next = NULL;
					}
					i++;
				}

			//Initializing all links (neighbours)
				vertexTmp = ret;
				//set links between vertices 0-5 48-53;
				//RIGHT LEFT
				i = 0;
				while(i<3) {
					vertexTmp[2*i].neighbour[RIGHT] = &vertexTmp[2*i+1];
					vertexTmp[48+ 2*i].neighbour[RIGHT] = &vertexTmp[48+ 2*i +1];
					vertexTmp[2*i+1].neighbour[LEFT] = &vertexTmp[2*i];
					vertexTmp[48+ 2*i+1].neighbour[LEFT] = &vertexTmp[48+ 2*i];
					i++;
				}

				i = 0;
				//UP DOWN
				while(i<2) {
					vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+3];
					vertexTmp[i+3].neighbour[UP] = &vertexTmp[i];
					
					vertexTmp[49+ i].neighbour[DOWN] = &vertexTmp[49+i+3];
					vertexTmp[49+i+3].neighbour[UP] = &vertexTmp[49+i];
					i++;
				}

				//sets links between 3rd - 2nd and 9th - 10th
				i = 0;
				while(i<4) {
					vertexTmp[2+i].neighbour[DOWN] = &vertexTmp[7+i];

					vertexTmp[7+i].neighbour[UP] = &vertexTmp[2+i];

					vertexTmp[43+i].neighbour[DOWN] = &vertexTmp[48+i];
					vertexTmp[48+i].neighbour[UP] = &vertexTmp[43+i];
					i++;
				}

				//sets links within 3rd
				i = 0;
				while(i<3) {
					vertexTmp[6+ 2*i].neighbour[RIGHT] = &vertexTmp[6 + 2*i +1];
					vertexTmp[6+ 2*i +1].neighbour[LEFT] = &vertexTmp[6 + 2*i];

					vertexTmp[42+ 2*i].neighbour[RIGHT] = &vertexTmp[42+ 2*i +1];
					vertexTmp[42+ 2*i +1].neighbour[LEFT] = &vertexTmp[42 + 2*i];
					i++;
				}

				//sets relation of 3rd and 9th with others
				i = 6;
				while(i<12) {
					vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
					vertexTmp[i+36].neighbour[UP] = &vertexTmp[i-6+36];
					i++;
				}

				//sets links of all others only UP DOWN
				i = 12;
				while(i<42) {
					vertexTmp[i].neighbour[UP] = &vertexTmp[i-6];
					vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
					i++;
				}


				i = 12;
				linkType = ODD_EVEN;
				while(i<42) {
					if(linkType == ODD_EVEN) {
						if(i%2!=0) {
							vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
							vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
						}
					}
					else {
						if(i%2==0) {
							vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
							vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
						}
					}
					i++;
					if(i%6==0) {
						linkType = !linkType;
					}
				}

			//some extra link breaking as the above while creates extra links
			//namely 17-18, 29-30 41-42 these links extend over the boundary
			//i.e right edge connecting to left edge of board

				vertexTmp = ret;
				vertexTmp[17].neighbour[RIGHT]=NULL;
				vertexTmp[29].neighbour[RIGHT]=NULL;
				vertexTmp[41].neighbour[RIGHT]=NULL;

				vertexTmp[18].neighbour[LEFT]=NULL;
				vertexTmp[30].neighbour[LEFT]=NULL;
				vertexTmp[42].neighbour[LEFT]=NULL;
		
			//intiailizing training centres
				i = 0;
				while(i<MAX_RETRAIN_CENTRES) {
					ret[trainingCentres[i]].training = trainingDisciplines[i];
					i++;
				}	
		
			allNodePaths(ret,allPaths);
			//initializing campuses
				i=0;
				vertexTmp = ret;
				while(i<NUM_NODES){
					vertexTmp[i].campus = getCampus(g,allPaths[i]);
					i++;
				}

			//initializing ARCs
				i=0;
				vertexTmp = ret;
				while(i<NUM_NODES){
					j=0;
					while(j<4){
						if(vertexTmp[i].neighbour[j] != NULL){
							ARCpath(ret,arc,allPaths[i],allPaths[vertexTmp[i].neighbour[j]->nodeID]);
							vertexTmp[i].ARC[j] = getARC(g,arc);
						}
						j++;
					}
					i++;
				}

			//FInal Return
				return ret;
		}

		static void allNodePaths(nodePtr origin,path* allPaths){
			pathStack p;
			PathStack paths = &p;
			nodePtr nTmp = NULL;
			initialize(paths,"");
			assert(paths!=NULL);
			path newPath;
			char tmp;
			int i;
			int state = START;

			//intiailizes allPaths to empty strings
			i=0;
			while(i<NUM_NODES){
				strcpy(allPaths[i],"RRRRR RRRRR RRR");
				i++;
			}

			while(isEmptyPaths(allPaths)==TRUE){
				transfer(paths,newPath);
				
				nTmp = goTo(origin,newPath,NULL);
				//printf("%d\n",nodeID);
				if(nTmp != NULL){
					#ifdef LOG_ALLPATHS
						if(nTmp->nodeID == 42)
						printf("ID:%d old %d new %d\n",nTmp->nodeID,(int)strlen(allPaths[nTmp->nodeID]),(int)strlen(newPath));
					#endif
					if(strlen(allPaths[nTmp->nodeID])>strlen(newPath))
					strcpy(allPaths[nTmp->nodeID],newPath);
				}

				if(nTmp == NULL){
					tmp = top(paths);
					if(tmp=='L'){
						pop(paths);
						push(paths,'R');
					}
					else{
						while(top(paths)=='R'){
							pop(paths);
						}
						if(size(paths)!=0){
							pop(paths);
							push(paths,'R');
						}
					}
				}

				else if(size(paths)==0){
					if(state == START){
						push(paths,'L');
						state = FORWARD;
					}
					else {
						state = EXIT;
					}
						
				}
				else if(size(paths)>=MAX_POSSIBLE_PATH){
					tmp = top(paths);
					if(tmp=='L'){
						pop(paths);
						push(paths,'R');
					}
					else{
						pop(paths);
						while(top(paths)=='R'){
							pop(paths);
						}
						if(size(paths)>0){
							pop(paths);
							push(paths,'R');
						}
					}
				}
				else{
					push(paths,'L');
				}
				#ifdef LOG_ALLPATHS
					path t;
					transfer(paths,t);
					printf("%s\n",t);
				#endif
			}		
		}
	
		static nodePtr goTo(nodePtr origin,char* path,int* lastDir) {
			
			nodePtr traveller = NULL;
			char* pathDir = path;
			int right = FALSE,it = 0,last=DOWN;
			int tmp,state = VALUE_FOUND;

			int dir1,dir2,dir3;

			traveller = origin;
			if(!(*pathDir == 'R' || *pathDir == 'L' ||*pathDir == '\0')){
				traveller = NULL;
				state = VALUE_NOTFOUND;
			}

			//the first 'shift' is done at origin, 
			//this is straightforward as the relative right 'R' is the left neighbour when you look form above
			if(*pathDir == 'R') {
				//the direction of neighbour is opp of the pathDir
				traveller = traveller->neighbour[DOWN];
				last = DOWN;
				pathDir++;
			}
			else if(*pathDir == 'L'){
				traveller = traveller->neighbour[RIGHT];
				last = RIGHT;
				pathDir++;	
			}

			

			tmp = -1;

			//after the first shift it has to enter a while to 
			//get through the remaining path 
			while(*pathDir!='\0' && state == VALUE_FOUND) {
				state = VALUE_NOTFOUND;
				assert(*pathDir == 'R' || *pathDir == 'L' ||*pathDir == 'B' );
				//it is iterator and it will start from the opposite of last
				//as last was relative to the previous vertex 
				it = oppositeDir(last);
				right = FALSE;
				if(*pathDir == 'B') {
					tmp = oppositeDir(last);
					state = VALUE_FOUND;
				}
				else{
					it = (it+1)%4;
					if(traveller->neighbour[it]!=NULL){
						right = TRUE;
						dir1 = TRUE;
						if(*pathDir == 'R'){
							tmp = it;
							state = VALUE_FOUND;
						}
					}
					it = (it+1)%4;
					if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
						dir2= TRUE;
						if(right == TRUE){
							//left
							if(*pathDir == 'L'){
								tmp = it;
								state = VALUE_FOUND;
							}
						}
						else {
							right = TRUE;
							if(*pathDir == 'R'){
								tmp = it;
								state = VALUE_FOUND;
							}
						}
					}
					it = (it+1)%4;
					if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
						//left
						dir3 = TRUE;
						if(*pathDir == 'L'){
							tmp = it;
							state = VALUE_FOUND;
						}
					}
					it = oppositeDir(last);
					dir1 = dir2 = dir3 = FALSE;
					if(traveller->neighbour[(it+1)%4]!=NULL)dir1 = TRUE;
					if(traveller->neighbour[(it+2)%4]!=NULL)dir2 = TRUE;
					if(traveller->neighbour[(it+3)%4]!=NULL)dir3 = TRUE;
					if(dir1 == FALSE && dir2 == TRUE && dir3==FALSE){
						if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+3)%4]!=NULL){
							if(*pathDir == 'R'){
								tmp = last;
								state = VALUE_FOUND;
							}
							else {
								state = VALUE_NOTFOUND;
							}
						}
						else if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+1)%4]!=NULL){
							if(*pathDir == 'L'){
								tmp = last;
								state = VALUE_FOUND;
							}
							else {
								state =VALUE_NOTFOUND;
							}	
						}
						else {
							state = VALUE_NOTFOUND;
						}
					}
				}

				if(state == VALUE_FOUND){
					traveller = traveller->neighbour[tmp];
					last = tmp;
					pathDir++;
				}
				else{
					traveller = NULL;
				}
			}
			if(state == VALUE_NOTFOUND){
				traveller = NULL;
			}

			if(lastDir != NULL) {
				*lastDir = last;
			}

			return traveller;
		}

	//tools and weapons
		static int isEmptyPaths(path* paths){
			int i=1;
			int ret = FALSE;
			while(i<NUM_NODES && ret == FALSE){
				if(strlen(paths[i])>MAX_POSSIBLE_PATH){
					ret = TRUE;
				}
				i++;
			}
			return ret;
		}

		static int oppositeDir(int i) {
			assert(i>=0&&i<4);
			int ret;
			ret = i+2;
			if(ret>=4) {
				ret-=4;
			}

			return ret;
		}

		static void ARCpath(nodePtr origin,path ret,path from,path to){
			int last;
			nodePtr start = goTo(origin,from,&last);
			assert(start != NULL);
			int i,j,size = strlen(from);
			int right;
			nodePtr end = goTo(origin,to,NULL);
			path tmp;
			strcpy(tmp,from);
			strcpy(ret,"");

			i= UP;
			while(start->neighbour[i]!= end && i<RIGHT){
				i++;
			}
			if(start->neighbour[i] == end){

				
				right = FALSE;
				j= oppositeDir(last);
				if(j==i){
					strcpy(ret,from);
					ret[size] = 'B';
					ret[size+1] = '\0';
				}
				else{
					j= (j+1)%4;
					if(start->neighbour[j]!=NULL){
						right = TRUE;
						if(j==i){
							strcpy(ret,from);
							ret[size] = 'R';
							ret[size+1] = '\0';
						}
					}
					j = (j+1)%4;
					if(start->neighbour[j]!=NULL){
						if(right == TRUE){
							if(j==i){
								strcpy(ret,from);
								ret[size] = 'L';
								ret[size+1] = '\0';
							}
						}
						else{
							right = TRUE;
							if(j==i){
								strcpy(ret,from);
								ret[size] = 'R';
								ret[size+1] = '\0';
							}
						}
					}
					j=(j+1)%4;
					if(start->neighbour[j]!=NULL){
						if(j==i){
							strcpy(ret,from);
							ret[size] = 'L';
							ret[size+1] = '\0';
						}
					}
				}
			}
		}

		static action newAction(int code,path destination,int disciplineFrom,int disciplineTo){
			action ret;
			ret.actionCode = code;
			strcpy(ret.destination,destination);
			ret.disciplineFrom = disciplineFrom;
			ret.disciplineTo = disciplineTo;

			return ret;
		}

	//random Functions
		static action randomCampus(Game g,nodePtr origin){
			int i,j;
			path allPaths[NUM_NODES];
			allNodePaths(origin,allPaths);
			action ret = newAction(PASS,"",0,0);
			action tmp;
			srand(time(NULL));

			int random = START_RAND + rand()%RANGE_RAND;
			int state = POSSIBLE;
			i=0;
			while(i<random && state != ONLY_PASS){
				j=0;
				while(j<NUM_NODES){
					tmp = newAction(BUILD_CAMPUS,allPaths[j],0,0);
					if(isLegalAction(g,tmp)==TRUE){
						i++;
						ret = tmp;
					}
					j++;
				}
				if(i==0){
					state = ONLY_PASS;
				}
			}

			if(ret.actionCode != PASS){MARK}
			return ret;
		}

		static action randomARC(Game g,nodePtr origin){
			int i,j,k;
			path allPaths[NUM_NODES];
			nodePtr nTmp = origin;
			allNodePaths(origin,allPaths);
			path arc;
			action ret = newAction(PASS,"",0,0);
			action tmp;
			srand(time(NULL));

			int random = START_RAND + rand()%RANGE_RAND;
			int state = POSSIBLE;
			i=0;
			while(i<random && state != ONLY_PASS){
				j=0;
				while(j<NUM_NODES){
					
					k=0;
					while(k<4){
						if(nTmp[j].neighbour[k]!=NULL){
							ARCpath(origin,arc,allPaths[j],allPaths[nTmp[j].neighbour[k]->nodeID]);
							tmp = newAction(OBTAIN_ARC,arc,0,0);
							if(isLegalAction(g,tmp)==TRUE){
								//MARK
								i++;
								ret = tmp;
							}
						}
						k++;
					}
					j++;
				}
				if(i==0){
					state = ONLY_PASS;
				}
			}

			return ret;
		}

		static action randomGO8(Game g,nodePtr origin){
			int i,j;
			path allPaths[NUM_NODES];
			allNodePaths(origin,allPaths);
			action ret = newAction(PASS,"",0,0);
			action tmp;
			srand(time(NULL));

			int random = START_RAND + rand()%RANGE_RAND;
			int state = POSSIBLE;
			i=0;
			while(i<random && state != ONLY_PASS){
				j=0;
				while(j<NUM_NODES){
					tmp = newAction(BUILD_GO8,allPaths[j],0,0);
					if(isLegalAction(g,tmp)==TRUE){
						i++;
						tmp = ret;
					}
					j++;
				}
				if(i==0){
					state = ONLY_PASS;
				}
			}

			return ret;
		}

//#################################################################
//SIJIA'S FUNCTION
//#################################################################
   //pathStack:
		static char top(PathStack s){
		    char ret;
		    if(s->size > 0)
		    ret = s->items[(s->size) - 1];
				else ret = '\0';
				return ret;
		}

		static int size(PathStack s){
		    return (s->size);
		}

		static void push(PathStack s, char c){
		    s->items[(s->size)]=c;
		    (s->size)++;
		    s->items[s->size] = '\0';
		}

		static char pop(PathStack s){
				assert(s->size != 0);
				char ret = top(s);
		    s->items[(s->size)-1]='\0';
		    (s->size)--;
		    return ret;
		}

#ifdef NOT_SUBMITTING
//#################################################################
//TESTS
//#################################################################
	static void testOppositeDir() {
		assert(oppositeDir(UP)==DOWN);
		assert(oppositeDir(DOWN)==UP);
		assert(oppositeDir(LEFT)==RIGHT);
		assert(oppositeDir(RIGHT)==LEFT);
	}

	static void testGoTo() {

		int something;
  	int disciplines[] = DEFAULT_DISCIPLINES;
  	int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g); 
		assert(goTo(origin,"LL",NULL)==NULL);
		assert(goTo(origin,"LR",NULL)->nodeID==4);
		
		assert(goTo(origin,"LLRR",NULL)==NULL);
		assert(goTo(origin,"LRLL",NULL)==NULL);
		assert(goTo(origin,"BBLR",NULL)==NULL);
		assert(goTo(origin,"RLRLRLRRLR",NULL)!=NULL);
		assert(goTo(origin,"RLRLRLRRLR",NULL)->nodeID == 42);
		assert(goTo(origin,"LRLR",NULL)->nodeID==10);
		assert(goTo(origin,"RRL",NULL)->nodeID == 7);
		assert(goTo(origin,"RRLRL",NULL)->nodeID == 12);
		assert(goTo(origin,"RB",NULL)->nodeID == 0);
		assert(goTo(origin,"RLRLRLRLRL",NULL)->nodeID == 52);
		assert(goTo(origin,"LRL",NULL)->nodeID == 5);
		assert(goTo(origin,"RLR",&something)->nodeID == 14);

		assert(something == DOWN);
		assert(goTo(origin,"LRL",&something)->nodeID == 5);
		assert(something == RIGHT);

		assert(goTo(origin,"L",NULL)->nodeID == 1);
		assert(goTo(origin,"R",NULL)->nodeID == 3);
		assert(goTo(origin,"LRRLRLRLRLR",NULL)->nodeID==53);

		assert(goTo(origin,"LRRLRLRLRLRBLLRLRLL",NULL)->nodeID==36);
		assert(goTo(origin,"LRRLRLRLRLRBLLRLRLLL",NULL)->nodeID==42);
		assert(goTo(origin,"LRRLRLRLRLRBLLRLRLLR",NULL)==NULL);

		disposeGame(g);
		free(origin);
	}

	static void testAllNodePaths(){

		int i;
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);
		path array[54];


		allNodePaths(origin,array);
		assert(strcmp(array[0],"")==0);
		assert(strcmp(array[1],"L")==0);
		assert(strcmp(array[3],"R")==0);
		assert(strcmp(array[3],"R")==0);
		i=0;
		while(i<NUM_NODES){
			assert(goTo(origin,array[i],NULL)->nodeID == i);
			i++;
		}

		disposeGame(g);
		free(origin);
	}

	static void testStack(){
		pathStack p;
		PathStack s = &p;
		initialize(s,"RL");
		char str[40];
		assert(strcmp(s->items,"RL")==0);
		assert(s->size == 2);

		push(s,'R');
		assert(strcmp(s->items,"RLR")==0);
		assert(s->size == 3);

		pop(s);
		assert(strcmp(s->items,"RL")==0);
		assert(s->size == 2);

		transfer(s,str);
		assert(strcmp(str,"RL")==0);
		assert(s->size == 2);
		assert(size(s)==2);
	}

	static void testShortestDist(){
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);

		assert(shortestDist(origin,"","R")==1);
		assert(shortestDist(origin,"","L")==1);
		assert(shortestDist(origin,"","LRRRR")==1);

		assert(shortestDist(origin,"R","R")==0);
		assert(shortestDist(origin,"RLLLL","L")==0);
		assert(shortestDist(origin,"LRR","LRRRR")==2);
		
		assert(shortestDist(origin,"R","LR")==3);
		assert(shortestDist(origin,"RR","LRL")==5);
		assert(shortestDist(origin,"RRLRL","LRLRLR")==11);
		


		disposeGame(g);
		free(origin);
	}

	static void testARCpath(){
		int disciplines[] = DISCIPLINES_BPS;
		int dice[] = DICE_4;
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);
		path array[NUM_NODES];
		allNodePaths(origin,array);

		path ret;
		ARCpath(origin,ret,array[0],array[1]);
		assert(strcmp(ret,"L")==0);

		ARCpath(origin,ret,array[0],array[4]);
		assert(strcmp(ret,"")==0);

		ARCpath(origin,ret,array[0],array[3]);
		assert(strcmp(ret,"R")==0);

		ARCpath(origin,ret,array[4],array[5]);
		assert(strcmp(ret,"LRL")==0);

		ARCpath(origin,ret,array[4],array[9]);
		assert(strcmp(ret,"LRR")==0);

		ARCpath(origin,ret,array[4],array[1]);
		assert(strcmp(ret,"LRB")==0);
	}

	static void testValueNode(){
		int disciplines[] = DISCIPLINES_BPS;
		int dice[] = DICE_4;
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);
		path array[NUM_NODES];
		allNodePaths(origin,array);
		throwDice(g,1);

		assert(valueNode(g,origin,array[6],BUILD_CAMPUS)==SCORE_BPS);
		assert(valueNode(g,origin,array[2],BUILD_CAMPUS)==SCORE_BPS + SCORE_MAX - SCORE_MTV);
		assert(valueNode(g,origin,array[0],BUILD_CAMPUS)==0);

		assert(valueNode(g,origin,array[10],BUILD_CAMPUS)==SCORE_BPS*2);
		assert(valueNode(g,origin,array[15],BUILD_CAMPUS)==SCORE_BPS*3);
		assert(valueNode(g,origin,array[6],BUILD_CAMPUS)==SCORE_BPS);


		disposeGame(g);
		free(origin);
	}

	static void testValueARC(){
		int disciplines[] = {
			1,1,1,
			1,1,1,1,
			0,0,0,0,0,
			0,0,0,0,
			0,0,0
		};
		int dice[] = {
			1,1,1,
			1,1,1,1,
			1,1,1,1,1,
			1,1,1,1,
			1,1,1
		};
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);

		assert(valueARC(g,origin,"L")<valueARC(g,origin,"R"));

		disposeGame(g);
		free(origin);
	}

	static void testNewAction(){
		action ret;
		ret = newAction(1,"123",3,7);
		assert(ret.actionCode == 1);
		assert(strcmp(ret.destination,"123")==0);
		assert(ret.disciplineFrom == 3);
		assert(ret.disciplineTo == 7);
	}

	static void testValuableNode(){
		path allPaths[NUM_NODES];
		path arc;

		int disciplines[] = {
			1,1,1,
			1,0,5,1,
			1,0,0,0,2,
			0,0,0,0,
			0,0,0
		};
		int dice[] = {
			1,1,1,
			1,1,1,1,
			3,1,1,1,3,
			1,1,1,1,
			1,1,1
		};
		Game g = newGame(disciplines,dice);
		nodePtr origin = newNetwork(g);
		throwDice(g,3);
		action a = valuableNode(g,origin);
		assert(a.actionCode == PASS);
		a = newAction(OBTAIN_ARC,"R",0,0);
		makeAction(g,a);
		a = newAction(OBTAIN_ARC,"L",0,0);
		makeAction(g,a);

		assert(getARC(g,"L")==UNI_A);
		assert(getARC(g,"R")==UNI_A);

		a = valuableNode(g,origin);
		assert(a.actionCode == PASS);
		
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);



		a = valuableNode(g,origin);
		assert(a.actionCode == PASS);
		a = newAction(OBTAIN_ARC,"RR",0,0);
		makeAction(g,a);
		a = newAction(OBTAIN_ARC,"LR",0,0);
		makeAction(g,a);
		a = newAction(OBTAIN_ARC,"LRL",0,0);
		makeAction(g,a);	
		
		a = valuableNode(g,origin);

		assert(getARC(g,"L")==UNI_A);
		assert(getARC(g,"R")==UNI_A);
		assert(getARC(g,"LR")==UNI_A);
		assert(getARC(g,"RR")==UNI_A);
		assert(getARC(g,"LRL")==UNI_A);
		assert(a.actionCode == BUILD_CAMPUS);
		assert(strcmp(a.destination,"RR")==0);
		
		disposeGame(g);
		free(origin);

		//testing C

		int disciplines2[] = {
			1,1,2,
			1,1,1,1,
			0,0,0,0,0,
			1,2,0,0,
			1,0,0
		};
		int dice2[] = {
			1,1,8,
			1,1,1,1,
			3,1,1,1,3,
			1,1,1,1,
			8,1,1
		};

		g = newGame(disciplines2,dice2);
		origin = newNetwork(g);
		allNodePaths(origin,allPaths);

		//TERRA NULLIS
		throwDice(g,8);
		//UNI_A

		throwDice(g,8);
		throwDice(g,8);
		//UNI_C

		int i =0;
		while(i<15){
			throwDice(g,8);
			throwDice(g,8);
			throwDice(g,8);
			i++;
		}
		//still UNI_C's turn
		assert(getWhoseTurn(g)==UNI_C);

		ARCpath(origin,arc,allPaths[11],allPaths[17]);
		
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);
		ARCpath(origin,arc,allPaths[11],allPaths[10]);
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);
		
		ARCpath(origin,arc,allPaths[42],allPaths[36]);
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);
		
		ARCpath(origin,arc,allPaths[42],allPaths[43]);
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);
		
		a = valuableNode(g,origin);
		assert(a.actionCode == PASS);

		ARCpath(origin,arc,allPaths[36],allPaths[30]);
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);

		a = valuableNode(g,origin);
		assert(a.actionCode == BUILD_CAMPUS);
		assert(goTo(origin,a.destination,NULL)->nodeID == 30);

		ARCpath(origin,arc,allPaths[10],allPaths[16]);
		assert(strcmp(arc,"")!=0);
		a = newAction(OBTAIN_ARC,arc,0,0);
		makeAction(g,a);

		a = valuableNode(g,origin);
		printf("<%ld %ld>\n",valueNode(g,origin,allPaths[16],BUILD_CAMPUS),valueNode(g,origin,allPaths[30],BUILD_CAMPUS));
		assert(a.actionCode == BUILD_CAMPUS);
		assert(goTo(origin,a.destination,NULL)->nodeID == 16);

		disposeGame(g);
		free(origin);
	}

	static void testValuableARC(){
		nodePtr origin;
		Game g;
		action a;
		path allPaths[NUM_NODES];

		int lastDir;
		int disciplines1[] = {
			0,0,1,
			0,0,1,1,
			1,0,0,0,2,
			0,0,0,0,
			0,0,0
		};
		int dice1[] = {
			1,1,1,
			1,1,1,1,
			3,1,1,1,3,
			1,1,1,1,
			1,1,1
		};

		g = newGame(disciplines1,dice1);
		origin = newNetwork(g);
		allNodePaths(origin,allPaths);
		throwDice(g,3);

		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		throwDice(g,3);
		assert(getWhoseTurn(g)==UNI_A);

		a = valuableARC(g,origin);
		printf("\n<<%d>>\n",goTo(origin,a.destination,&lastDir)->nodeID);
		assert(a.actionCode == OBTAIN_ARC);
		makeAction(g,a);

		a = valuableARC(g,origin);
		printf("<<%d>>\n",goTo(origin,a.destination,&lastDir)->nodeID);
		assert(a.actionCode == OBTAIN_ARC);

		a = valuableARC(g,origin);		
		while(a.actionCode == OBTAIN_ARC){
			makeAction(g,a);
			printf("<<%d>>\n",goTo(origin,a.destination,&lastDir)->nodeID);
			a = valuableARC(g,origin);
			
		}
		

		free(origin);
		disposeGame(g);
	}

//#################################################################
//PRINT
//#################################################################
	//Network
		static void printNetwork(nodePtr origin){
			int i,j,k,area;
			int training;
			int grid[GRID_ROW][GRID_COL];
			i=0;
			while(i<GRID_ROW){
				j=0;
				while(j<GRID_COL){
					grid[i][j]=0;
					j++;
				}
				i++;
			}
			grid[0][0] = -1;
			grid[0][1] = -1;
			grid[0][4] = -1;
			grid[0][5] = -1;

			grid[1][0] = -1;
			grid[1][5] = -1;


			grid[10][0] = -1;
			grid[10][1] = -1;
			grid[10][4] = -1;
			grid[10][5] = -1;

			grid[9][0] = -1;
			grid[9][5] = -1;

			i=0;k=0;
			while(i<GRID_ROW){
				j=0;
				while(j<GRID_COL){
					if(grid[i][j]!= -1){
						grid[i][j] = k++;
					}
					j++;
				}
				i++;
			}

			area = UPPER;
			i=0;
			while(i<GRID_ROW){
				j=0;
				while(j<GRID_COL){
					if(grid[i][j]!=-1){
						if(area==UPPER){
							training = origin[grid[i][j]].training;
							if(training!=NO_TRAINING){
								printf(" %d",training);
							}
							else {
								printf("  ");
							}
							if(origin[grid[i][j]].neighbour[UP]!=NULL)
								{printf("%c", EMPTY_V);}
							else
								{printf(" ");}
							
							
							if(training!=NO_TRAINING){
								printf("%d ",training);
							}
							else {
								printf("  ");
							}
						}
						else if(area==MIDDLE){
							if(origin[grid[i][j]].neighbour[LEFT]!=NULL)
								{printf("%c%c", EMPTY_H, EMPTY_H);}
							else
								{printf("  ");}

							printf(" ");

							if(origin[grid[i][j]].neighbour[RIGHT]!=NULL)
								{printf("%c%c", EMPTY_H, EMPTY_H);}
							else
								{printf("  ");}
						}
						else if(area==LOWER){
							if(origin[grid[i][j]].neighbour[DOWN]!=NULL)
								{printf("  %c  ", EMPTY_V);}
							else
								{printf("     ");}
						}
					}
					else {
						printf("     ");
					}
					
					j++;	
				}
				area = (area+1)%3;
				printf("\n");
				if(area==UPPER)
				{i++;}
			}
		}
#endif
